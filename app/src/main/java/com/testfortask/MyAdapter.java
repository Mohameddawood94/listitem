package com.testfortask;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mohamed on 4/27/17.
 */

public class MyAdapter extends BaseAdapter {

    Context context;
    int inflaterId;
    List<Model> modelList = new ArrayList<>();

    public MyAdapter(Context context, int inflaterId, List<Model> modelList) {
        this.context = context;
        this.inflaterId = inflaterId;
        this.modelList = modelList;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int i) {
        return modelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View root = view;
        ViewHolder holder;
        if (root == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            root = inflater.inflate(inflaterId, viewGroup, false);
          //  holder = new ViewHolder(root);
           // root.setTag(holder);

        }else {
            holder = (ViewHolder) root.getTag();
        }

        if (i==0){
            // do something
        }else if (i==1){
            // do something
        }

        return root;
    }

    class ViewHolder {
        TextView textView;
        TextView te2xtView;
        TextView t1extView;

        public ViewHolder(View root) {
            textView = (TextView) root.findViewById(R.id.text1);
            te2xtView = (TextView) root.findViewById(R.id.text2);
            t1extView = (TextView) root.findViewById(R.id.text3);
        }
    }
}
