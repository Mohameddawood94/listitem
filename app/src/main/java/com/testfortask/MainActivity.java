package com.testfortask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    List<Model> modelList = new ArrayList<>();
    ListView listView;
    MyAdapter myAdapter;

    private static final String TAG = "RxAndroidSamples";
    private final CompositeDisposable disposables = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.myList);
       // new GtData().execute();
        onRunSchedulerExampleButtonClicked();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }

    void onRunSchedulerExampleButtonClicked() {
        disposables.add(sampleObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onComplete()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError()", e);
                    }

                    @Override
                    public void onNext(String string) {
                        Log.d(TAG, "onNext(" + string + ")");
                    }
                }));
    }

    static Observable<String> sampleObservable() {
        return Observable.defer(new Callable<ObservableSource<? extends String>>() {
            @Override
            public ObservableSource<? extends String> call() throws Exception {
                // Do some long running operation
                SystemClock.sleep(5000);
                return Observable.just("one", "two", "three", "four", "five");
            }
        });
    }


    String url = "http://164.132.145.149:7019/elk/elkservice?action=invoices&cid=1";

    private List<Model> getData() {
        List<Model> modelList = new ArrayList<>();
        try {
            String result = DataConnection.sendGet(url);


            JsonElement jsonElement = new JsonParser().parse(result);
            JsonArray jsonArray = jsonElement.getAsJsonArray();

            //TODO note //////////////////////////////////////////////////////////////////////////////
            JsonObject object2 = jsonElement.getAsJsonObject();
            JsonArray jsonArray1 = object2.getAsJsonArray("array name if exists if no name let it double quotes");
            for (int i = 0; i < jsonArray1.size(); i++) {
                JsonObject object = jsonArray1.get(i).getAsJsonObject();
                // object.get("hello key 1").getAsBoolean();
                // object.get("hello key 2").getAsInt();
                // object.get("hello key 3").getAsString();
            }
            /////////////////////////////////////////////////////////////////////////////////

            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject object = jsonArray.get(i).getAsJsonObject();
                Model model = new Model();
                model.setNumber(object.get("number").getAsInt());
                model.setRate(object.get("rate").getAsInt());
                model.setTotal(object.get("total").getAsInt());
                model.setPtype(object.get("ptype").getAsInt());
//                model.setStatus(object.get("status'").getAsInt());
                model.setAccountnumber(object.get("accountnumber").getAsInt());
                model.setBkname(object.get("bkname").getAsString());
                model.setDate(object.get("date").getAsString());
                model.setBranch(object.get("branch").getAsString());
                modelList.add(model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelList;
    }

    class GtData extends AsyncTask<Void, String, List<Model>> {

        @Override
        protected List<Model> doInBackground(Void... voids) {
            modelList = getData();
            return modelList;
        }

        @Override
        protected void onPostExecute(List<Model> models) {
            super.onPostExecute(models);
            myAdapter = new MyAdapter(MainActivity.this, R.layout.list_row_item, modelList);
            listView.setAdapter(myAdapter);
        }
    }
}
